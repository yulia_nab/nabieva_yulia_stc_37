package ru.innopolis.stc37.nabieva.projecttask2.app;

import ru.innopolis.stc37.nabieva.projecttask2.dao.CustomDataSource;
import ru.innopolis.stc37.nabieva.projecttask2.dto.StatisticDto;
import ru.innopolis.stc37.nabieva.projecttask2.repository.GamesRepository;
import ru.innopolis.stc37.nabieva.projecttask2.repository.GamesRepositoryJdbcImpl;
import ru.innopolis.stc37.nabieva.projecttask2.repository.PlayersRepository;
import ru.innopolis.stc37.nabieva.projecttask2.repository.PlayersRepositoryJdbcImpl;
import ru.innopolis.stc37.nabieva.projecttask2.repository.ShotsRepository;
import ru.innopolis.stc37.nabieva.projecttask2.repository.ShotsRepositoryJdbcImpl;
import ru.innopolis.stc37.nabieva.projecttask2.services.GameService;
import ru.innopolis.stc37.nabieva.projecttask2.services.GameServiceImpl;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.Random;
import java.util.Scanner;

public class MainJDBC {

    public static final String JDBC_URL = "jdbc:postgresql://localhost:5432/stc37_yulia";
    public static final String JDBC_USER = "postgres";
    public static final String JDBC_PASSWORD = "5858989";


    public static void main(String[] args) throws SQLException {


        DataSource dataSource = new CustomDataSource(JDBC_USER, JDBC_PASSWORD, JDBC_URL);
        PlayersRepository playersRepository = new PlayersRepositoryJdbcImpl(dataSource);
        ShotsRepository shotsRepository = new ShotsRepositoryJdbcImpl(dataSource);
        GamesRepository gamesRepository = new GamesRepositoryJdbcImpl(dataSource);

        GameService gameService = new GameServiceImpl(playersRepository, gamesRepository, shotsRepository);

        Scanner scanner = new Scanner(System.in);

        String first = scanner.nextLine();
        String second = scanner.nextLine();
//        language=SQL
//        String insertFirstPlayer = "insert into" + "player(id, ip, name, points, maxwinscount, maxlosescou) values (?,?,?,?,?,?)";
//        String insertSecondPlayer = "insert into" + " player(id, ip, name, points, maxwinscount, maxlosescount) values (?,?,?,?,?,?)";
//
//
//        statement.executeUpdate(insertFirstPlayer);
//        statement.executeUpdate(insertSecondPlayer);


        Random random = new Random();

        Long gameId = gameService.startGame("127.0.0.1", "127.0.0.2", first, second);
        String shooter = first;
        String target = second;
        int i = 0;
        while (i < 10) {

            System.out.println(shooter + " делайте выстрел в " + target);
            scanner.nextLine();

            int success = random.nextInt(2);

            if (success == 0) {
                System.out.println("Успешно!");
                gameService.shot(gameId, shooter, target);
            } else {
                System.out.println("Промах!");
            }

            String temp = shooter;
            shooter = target;
            target = temp;
            i++;
        }

        StatisticDto statistic = gameService.finishGame(gameId);

        System.out.println(statistic);


    }

}
