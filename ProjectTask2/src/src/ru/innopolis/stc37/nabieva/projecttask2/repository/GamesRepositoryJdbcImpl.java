package ru.innopolis.stc37.nabieva.projecttask2.repository;

import ru.innopolis.stc37.nabieva.projecttask2.models.Game;
import ru.innopolis.stc37.nabieva.projecttask2.models.Player;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.time.LocalDateTime;

import static ru.innopolis.stc37.nabieva.projecttask2.dao.JdbcUtil.closeJdbcObjects;

public class GamesRepositoryJdbcImpl implements GamesRepository {

    //language=SQL
    private static final String SQL_INSERT_GAME = "insert into game(datetime, playerfirst, playersecond, playerfirstshotscount, playersecondshotscount, secondsgametimeamount) values (?,?, ?, ?, ?, ?)";
    //language=SQL
    private static final String SQL_FIND_BY_ID = "select * from game where id = ?";

    //language=SQL
    private static final String SQL_UPDATE_GAME_FIND_BY_ID = "update game set" +
            " id = ?, datetime = ?, playerfirst = ?, playersecond = ?, playerfirstshotscount = ?, playersecondshotscount = ?, secondsgametimeamount = ? where id = ?";

    static private final RowMapper<Game> gameRowMapper = row -> Game.builder()
            .id(row.getLong("id"))
            .dateTime(LocalDateTime.parse(row.getString("datetime")))
            .playerFirst(Player.builder()
                    .id(row.getLong("player_first"))
                    .build())
            .playerSecond(Player.builder()
                    .id(row.getLong("player_second"))
                    .build())
            .build();


    private DataSource dataSource;


    public GamesRepositoryJdbcImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void save(Game game) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet generatedId = null;

        try {
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(SQL_INSERT_GAME, Statement.RETURN_GENERATED_KEYS);
            statement.setString(1,game.getDateTime().toString());
            statement.setLong(2, game.getPlayerFirst().getId());
            statement.setLong(3, game.getPlayerSecond().getId());
            statement.setInt(4, game.getPlayerFirstShotsCount());
            statement.setInt(5, game.getPlayerSecondShotsCount());
            statement.setLong(6, game.getSecondsGameTimeAmount());
            statement.executeUpdate();
            int affectedRows = statement.executeUpdate();
            if (affectedRows != 1) {
                throw new SQLException("Can't insert data");
            }
            generatedId = statement.getGeneratedKeys();
            if (generatedId.next()) {
                game.setId(generatedId.getLong("id"));
            } else {
                throw new SQLException("Can't retrieve id");
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        } finally {
            closeJdbcObjects(statement, generatedId, connection);
        }
    }

    @Override
    public Game findById(Long gameId) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet rows = null;
        Game game = null;

        try {
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(SQL_FIND_BY_ID);
            statement.setLong(1, gameId);

            rows = statement.executeQuery();
            if (rows.next()) {
                game = gameRowMapper.mapRow(rows);
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        } finally {
            closeJdbcObjects(statement, rows, connection);
        }
        return game;
    }

    @Override
    public void update(Game game) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet generatedId = null;

        try {
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(SQL_UPDATE_GAME_FIND_BY_ID);

            statement.setTimestamp(1, Timestamp.valueOf(game.getDateTime()));
            statement.setString(2, game.getPlayerFirst().getName());
            statement.setString(3, game.getPlayerSecond().getName());
            statement.setInt(4, game.getPlayerFirstShotsCount());
            statement.setInt(5, game.getPlayerSecondShotsCount());
            statement.setLong(6, game.getSecondsGameTimeAmount());
            statement.setLong(7, game.getId());
            ;

            int affectedRows = statement.executeUpdate();
            if (affectedRows != 1) {
                throw new SQLException("Can't insert data");
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        } finally {
            closeJdbcObjects(statement, generatedId, connection);
        }
    }


}
