package ru.innopolis.stc37.nabieva.projecttask2.repository;

import ru.innopolis.stc37.nabieva.projecttask2.models.Player;

public interface PlayersRepository {
    Player findByNickname(String nickname);

    void save(Player player);

    void update(Player player);
}
