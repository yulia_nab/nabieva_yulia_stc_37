package ru.innopolis.stc37.nabieva.projecttask2.repository;

import ru.innopolis.stc37.nabieva.projecttask2.models.Shot;

public interface ShotsRepository {
    void save(Shot shot);
}
