package ru.innopolis.stc37.nabieva.projecttask2.services;

import ru.innopolis.stc37.nabieva.projecttask2.dto.StatisticDto;

public interface GameService {

    Long startGame(String firstIp, String secondIp, String firstPlayerNickname, String secondPlayerNickname);

    void shot(Long gameId, String shooterNickname, String targetNickname);

    StatisticDto finishGame(Long gameId);
}
