package ru.innopolis.stc37.nabieva.projecttask2.repository;

import ru.innopolis.stc37.nabieva.projecttask2.models.Shot;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;

import static ru.innopolis.stc37.nabieva.projecttask2.dao.JdbcUtil.closeJdbcObjects;


public class ShotsRepositoryJdbcImpl implements ShotsRepository {

    //language=SQL
    private static final String SQL_INSERT_SHOT = "insert into " +
            "shot(datetime, game, shooter, target) values (?, ?, ?, ?)";


    private DataSource dataSource;

    public ShotsRepositoryJdbcImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void save(Shot shot) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet generatedId = null;

        try {
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(SQL_INSERT_SHOT, Statement.RETURN_GENERATED_KEYS);
            statement.setTimestamp(1, Timestamp.valueOf(shot.getDateTime()));
            statement.setInt(2, Math.toIntExact(shot.getId()));
            statement.setString(3, shot.getShooter().getName());
            statement.setString(4, shot.getTarget().getName());
            statement.executeUpdate();

            int affectedRows = statement.executeUpdate();
            if (affectedRows != 1) {
                throw new SQLException("Can't insert data");
            }
            generatedId = statement.getGeneratedKeys();
            if (generatedId.next()) {
                shot.setId(generatedId.getLong("id"));
            } else {
                throw new SQLException("Can't retrieve id");
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        } finally {
            closeJdbcObjects(statement, generatedId, connection);
        }
    }

}
