package ru.innopolis.stc37.nabieva.projecttask2.repository;

import ru.innopolis.stc37.nabieva.projecttask2.models.Shot;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class ShotsRepositoryFileImpl implements ShotsRepository {
    private String dbFileName;
    private String sequenceFileName;

    public ShotsRepositoryFileImpl(String dbFileName, String sequenceFileName) {
        this.dbFileName = dbFileName;
        this.sequenceFileName = sequenceFileName;
    }

    @Override
    public void save(Shot shot) {
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(dbFileName, true));
            shot.setId(generateId());
            writer.write(shot.getId() + "#" + shot.getDateTime().toString() + "#" +
                    shot.getGame().getId() + "#" + shot.getShooter().getName() + "#" + shot.getTarget().getName() + "\n");
            writer.close();
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    private Long generateId() {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(sequenceFileName));
            String lastGeneratedIdAsString = reader.readLine();
            long id = Long.parseLong(lastGeneratedIdAsString);
            reader.close();

            BufferedWriter writer = new BufferedWriter(new FileWriter(sequenceFileName));
            writer.write(String.valueOf(id + 1));
            writer.close();

            return id;
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }
}
