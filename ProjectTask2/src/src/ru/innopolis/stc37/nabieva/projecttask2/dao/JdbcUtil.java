package ru.innopolis.stc37.nabieva.projecttask2.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class JdbcUtil {
    public static void closeJdbcObjects(PreparedStatement statement, ResultSet rows, Connection connection) {
        if (rows != null) {
            try {
                rows.close();
            } catch (SQLException ignore) {
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException ignore) {
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ignore) {
                }
            }
        }
    }
}