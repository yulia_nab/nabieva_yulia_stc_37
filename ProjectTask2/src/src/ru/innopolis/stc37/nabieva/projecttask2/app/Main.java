package ru.innopolis.stc37.nabieva.projecttask2.app;


import ru.innopolis.stc37.nabieva.projecttask2.dto.StatisticDto;
import ru.innopolis.stc37.nabieva.projecttask2.repository.GamesRepository;
import ru.innopolis.stc37.nabieva.projecttask2.repository.GamesRepositoryListImpl;
import ru.innopolis.stc37.nabieva.projecttask2.repository.PlayersRepository;
import ru.innopolis.stc37.nabieva.projecttask2.repository.PlayersRepositoryFilesImpl;
import ru.innopolis.stc37.nabieva.projecttask2.repository.PlayersRepositoryMapImpl;
import ru.innopolis.stc37.nabieva.projecttask2.repository.ShotsRepository;
import ru.innopolis.stc37.nabieva.projecttask2.repository.ShotsRepositoryFileImpl;
import ru.innopolis.stc37.nabieva.projecttask2.services.GameService;
import ru.innopolis.stc37.nabieva.projecttask2.services.GameServiceImpl;

import java.util.Random;
import java.util.Scanner;


public class Main {

    public static final String SHOTS_DB_TXT = "C:\\Users\\Yulia\\OneDrive\\Рабочий стол\\nab\\nabieva_yulia_stc_37\\ProjectTask2\\src\\src\\ru\\innopolis\\stc37\\nabieva\\projecttask2\\resources\\shots_db.txt";
    public static final String SHOTS_SEQUENCE_TXT = "C:\\Users\\Yulia\\OneDrive\\Рабочий стол\\nab\\nabieva_yulia_stc_37\\ProjectTask2\\src\\src\\ru\\innopolis\\stc37\\nabieva\\projecttask2\\resources\\shots_sequence.txt";
    public static final String PLAYERS_DB_TXT = "C:\\Users\\User\\Desktop\\28042021\\nabieva_yulia_stc_37\\ProjectTask2\\src\\stc\\ru\\innopolis\\stc37\\nabieva\\projecttask2\\resources\\players_db.txt";
    public static final String PLAYERS_ID_TXT = "C:\\Users\\User\\Desktop\\28042021\\nabieva_yulia_stc_37\\ProjectTask2\\src\\stc\\ru\\innopolis\\stc37\\nabieva\\projecttask2\\resources\\players_id.txt";

    public static void main(String[] args) {

        PlayersRepository playersRepository = new PlayersRepositoryMapImpl();
//        PlayersRepository playersRepository = new PlayersRepositoryFilesImpl(PLAYERS_DB_TXT, PLAYERS_ID_TXT);
        GamesRepository gamesRepository = new GamesRepositoryListImpl();
        ShotsRepository shotsRepository = new ShotsRepositoryFileImpl(SHOTS_DB_TXT,
                SHOTS_SEQUENCE_TXT);
        GameService gameService = new GameServiceImpl(playersRepository, gamesRepository, shotsRepository);

        Scanner scanner = new Scanner(System.in);

        String first = scanner.nextLine();
        String second = scanner.nextLine();
        Random random = new Random();

        Long gameId = gameService.startGame("127.0.0.1", "127.0.0.2", first, second);
        String shooter = first;
        String target = second;
        int i = 0;
        while (i < 10) {

            System.out.println(shooter + " делайте выстрел в " + target);
            scanner.nextLine();

            int success = random.nextInt(2);

            if (success == 0) {
                System.out.println("Успешно!");
                gameService.shot(gameId, shooter, target);
            } else {
                System.out.println("Промах!");
            }

            String temp = shooter;
            shooter = target;
            target = temp;
            i++;
        }

        StatisticDto statistic = gameService.finishGame(gameId);

        System.out.println(statistic);
    }
}
