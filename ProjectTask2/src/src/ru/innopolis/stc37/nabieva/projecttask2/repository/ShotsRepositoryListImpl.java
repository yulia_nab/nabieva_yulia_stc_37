package ru.innopolis.stc37.nabieva.projecttask2.repository;

import ru.innopolis.stc37.nabieva.projecttask2.models.Shot;

import java.util.ArrayList;
import java.util.List;

public class ShotsRepositoryListImpl implements ShotsRepository {

    private List<Shot> shots;

    public ShotsRepositoryListImpl() {
        this.shots = new ArrayList<>();
    }

    @Override
    public void save(Shot shot) {
        this.shots.add(shot);
    }
}