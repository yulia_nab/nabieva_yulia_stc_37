package ru.innopolis.stc37.nabieva.projecttask2.services;

import ru.innopolis.stc37.nabieva.projecttask2.dto.StatisticDto;
import ru.innopolis.stc37.nabieva.projecttask2.models.Game;
import ru.innopolis.stc37.nabieva.projecttask2.models.Player;
import ru.innopolis.stc37.nabieva.projecttask2.models.Shot;
import ru.innopolis.stc37.nabieva.projecttask2.repository.GamesRepository;
import ru.innopolis.stc37.nabieva.projecttask2.repository.PlayersRepository;
import ru.innopolis.stc37.nabieva.projecttask2.repository.ShotsRepository;

import java.time.Duration;
import java.time.LocalDateTime;

import static java.time.LocalDateTime.now;

public class GameServiceImpl implements GameService {

    private PlayersRepository playersRepository;

    private GamesRepository gamesRepository;

    private ShotsRepository shotsRepository;
    private LocalDateTime startGame = now();
    private LocalDateTime start = startGame;

    public GameServiceImpl(PlayersRepository playersRepository, GamesRepository gamesRepository, ShotsRepository shotsRepository) {
        this.playersRepository = playersRepository;
        this.gamesRepository = gamesRepository;
        this.shotsRepository = shotsRepository;
    }

    @Override
    public Long startGame(String firstIp, String secondIp, String firstPlayerNickname, String secondPlayerNickname) {

        System.out.println("ПОЛУЧИЛИ" + firstIp + " " + " " + secondIp  + " " + " " +  firstPlayerNickname  + " " + " " +  secondPlayerNickname);
        Player first = checkIfExists(firstIp, firstPlayerNickname);
        Player second = checkIfExists(secondIp, secondPlayerNickname);
        Game game = new Game(now(), first, second, 0, 0, 0L);
        gamesRepository.save(game);
        return game.getId();
    }

    private Player checkIfExists(String ip, String nickname) {
        Player player = playersRepository.findByNickname(nickname);
        if (player == null) {
            player = new Player(ip, nickname, 0, 0, 0);
            playersRepository.save(player);
        } else {
            player.setIp(ip);
            playersRepository.update(player);
        }
        return player;
    }

    @Override
    public void shot(Long gameId, String shooterNickname, String targetNickname) {
        Player shooter = playersRepository.findByNickname(shooterNickname);
        Player target = playersRepository.findByNickname(targetNickname);
        Game game = gamesRepository.findById(gameId);
        Shot shot = new Shot(now(), game, shooter, target);
        shooter.setPoints(shooter.getPoints() + 1);

        if (game.getPlayerFirst().getName().equals(shooterNickname)) {
            game.setPlayerFirstShotsCount(game.getPlayerFirstShotsCount() + 1);
        }
        if (game.getPlayerSecond().getName().equals(shooterNickname)) {
            game.setPlayerSecondShotsCount(game.getPlayerSecondShotsCount() + 1);
        }
        playersRepository.update(shooter);
        gamesRepository.update(game);
        shotsRepository.save(shot);
    }


    @Override
    public StatisticDto finishGame(Long gameId) {
        LocalDateTime end = now();
        Game game = gamesRepository.findById(gameId);
        Long id = game.getId();
        Player firstPlayer = game.getPlayerFirst();
        Integer firstPlayerScore = firstPlayer.getPoints();
        Player secondPlayer = game.getPlayerSecond();
        Integer secondPlayerScore = secondPlayer.getPoints();
        Integer firstPlayerHits = game.getPlayerFirstShotsCount();
        Integer secondPlayerHits = game.getPlayerSecondShotsCount();
        boolean firstPlayerWins = firstPlayerScore > secondPlayerScore;
        Long gameDuration = Duration.between(start, end).getSeconds();
        return new StatisticDto(id, firstPlayer, secondPlayer, firstPlayerHits, firstPlayerScore, secondPlayerHits, secondPlayerScore, firstPlayerWins, gameDuration);

    }
}
