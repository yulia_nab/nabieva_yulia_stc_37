package ru.innopolis.stc37.nabieva.projecttask2.repository;

import ru.innopolis.stc37.nabieva.projecttask2.models.Player;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import static ru.innopolis.stc37.nabieva.projecttask2.dao.JdbcUtil.closeJdbcObjects;


public class PlayersRepositoryJdbcImpl implements PlayersRepository {

    //language=SQL
    private static final String SQL_INSERT_PLAYER = "insert into " +
            "player(ip, name, points, maxwinscount, maxlosescount) values (?, ?, ?, ?, ?)";
    //language=SQL
    private static final String SQL_FIND_BY_NICKNAME = "select * from player where name = ?";
    //language=SQL
    private static final String SQL_UPDATE_BY_ID = "update player set " +
            " ip = ?, name = ?, points = ?, maxwinscount = ?, maxlosescount = ? where id = ?";
    private RowMapper<Player> playerRowMapper = row -> Player.builder()
            .id(row.getLong("id"))
            .ip(row.getString("ip"))
            .name(row.getString("name"))
            .build();

    private DataSource dataSource;

    public PlayersRepositoryJdbcImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public Player findByNickname(String nickname) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet rows = null;
        Player player = null;

        try {
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(SQL_FIND_BY_NICKNAME);
            statement.setString(1, nickname);

            rows = statement.executeQuery();
            if (rows.next()) {
                player = playerRowMapper.mapRow(rows);
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        } finally {
            closeJdbcObjects(statement, rows, connection);
        }
        return player;
    }

    @Override
    public void save(Player player) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet generatedId = null;

        try {
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(SQL_INSERT_PLAYER, Statement.RETURN_GENERATED_KEYS);
            statement.setString(1, player.getIp());
            statement.setString(2, player.getName());
            statement.setInt(3, player.getPoints());
            statement.setInt(4, player.getMaxWinsCount());
            statement.setInt(5, player.getMaxLosesCount());
            statement.executeUpdate();

            int affectedRows = statement.executeUpdate();

            if (affectedRows != 1) {
                throw new SQLException("Can't insert data");
            }
            generatedId = statement.getGeneratedKeys();
            if (generatedId.next()) {
                player.setId(generatedId.getLong("id"));
            } else {
                throw new SQLException("Can't retrieve id");
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        } finally {
            closeJdbcObjects(statement, generatedId, connection);
        }
    }

    @Override
    public void update(Player player) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet generatedId = null;

        try {
            connection = dataSource.getConnection();
            statement = connection.prepareStatement(SQL_UPDATE_BY_ID);
            statement.setString(1, player.getIp());
            statement.setString(2, player.getName());
            statement.setInt(3, player.getPoints());
            statement.setInt(4, player.getMaxWinsCount());
            statement.setInt(5, player.getMaxLosesCount());
            statement.setLong(6, player.getId());

            int affectedRows = statement.executeUpdate();

            if (affectedRows != 1) {
                throw new SQLException("Can't insert data");
            }

        } catch (SQLException e) {
            throw new IllegalStateException(e);
        } finally {
            closeJdbcObjects(statement, generatedId, connection);
        }
    }


}
