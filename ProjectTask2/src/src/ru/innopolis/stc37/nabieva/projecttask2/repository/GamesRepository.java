package ru.innopolis.stc37.nabieva.projecttask2.repository;

import ru.innopolis.stc37.nabieva.projecttask2.models.Game;

public interface GamesRepository {
    void save(Game game);

    Game findById(Long gameId);

    void update(Game game);
}
