package stc.ru.innopolis.stc37.nabieva.inno.inno.hashmap;
import stc.ru.innopolis.stc37.nabieva.inno.inno.Map;

public class HashMapImpl<K, V> implements Map<K, V> {
    private static final int DEFAULT_SIZE = 16;

    private MapEntry<K, V> entries[];

    public HashMapImpl() {
        this.entries = new MapEntry[DEFAULT_SIZE];
    }

    private static class MapEntry<K, V> {
        K key;
        V value;
        MapEntry<K, V> next;

        public MapEntry(K key, V value) {
            this.key = key;
            this.value = value;
        }
    }

    @Override
    public void put(K key, V value) {

        int index = key.hashCode() & (entries.length - 1);
        if (entries[index] != null) {
            MapEntry<K, V> current = entries[index];
            while (current != null) {

                if (current.key.equals(key)) {
                    current.value = value;
                    return;
                }
                current = current.next;
            }
            MapEntry<K, V> newMapEntry = new MapEntry<>(key, value);
            newMapEntry.next = entries[index];
            entries[index] = newMapEntry;
        } else {
            entries[index] = new MapEntry<>(key, value);
        }
    }

    @Override
    public V get(K key) {
        // TODO: реализовать
        int index = key.hashCode() & (entries.length - 1);
        return entries[index].value;
    }
}
