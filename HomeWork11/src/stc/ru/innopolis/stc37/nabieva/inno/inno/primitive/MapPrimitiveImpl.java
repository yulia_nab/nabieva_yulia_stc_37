package stc.ru.innopolis.stc37.nabieva.inno.inno.primitive;

import stc.ru.innopolis.stc37.nabieva.inno.inno.Map;

public class MapPrimitiveImpl<K, V> implements Map<K, V> {

    private static final int DEFAULT_SIZE = 10;

    private static class MapEntry<K, V> {
        K key;
        V value;

        public MapEntry(K key, V value) {
            this.key = key;
            this.value = value;
        }
    }
    private MapEntry<K, V> entries[];
    private int count;
    public MapPrimitiveImpl() {
        this.entries = new MapEntry[DEFAULT_SIZE];
        this.count = 0;
    }
    @Override
    public void put(K key, V value) {
        MapEntry<K, V> entry = new MapEntry<>(key, value);

        for (int i = 0; i < count; i++) {
            if (entries[i].key.equals(key)) {
                entries[i] = entry;
                return;
            }
        }

        this.entries[count] = entry;
        count++;
    }

    @Override
    public V get(K key) {
        for (int i = 0; i < count; i++) {
            if (entries[i].key.equals(key)) {
                return entries[i].value;
            }
        }
        return null;
    }
}
