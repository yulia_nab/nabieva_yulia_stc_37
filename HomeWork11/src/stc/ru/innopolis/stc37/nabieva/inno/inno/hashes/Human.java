package stc.ru.innopolis.stc37.nabieva.inno.inno.hashes;

import java.util.Objects;


public class Human {
    private String firstName;
    private String lastName;
    private int age;
    private double height;
    private boolean isWorker;

    public Human(String firstName, String lastName, int age, double height, boolean isWorker) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.height = height;
        this.isWorker = isWorker;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return age == human.age &&
                Double.compare(human.height, height) == 0 &&
                isWorker == human.isWorker &&
                Objects.equals(firstName, human.firstName) &&
                Objects.equals(lastName, human.lastName);
    }
    @Override
    public int hashCode() {
        return Objects.hash(firstName, lastName, age, height, isWorker);
    }
}
