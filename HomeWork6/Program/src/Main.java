public class Main {
    public static void main(String[] args) {
        RemoteController controller = new RemoteController();
        Tv tv = new Tv("Apple", 105);

        controller.setTv(tv);
        tv.setRemoteController(controller);

        Channel channel1 = new Channel("Channel1");
        Channel channel2 = new Channel("Channel2");
        Channel channel3 = new Channel("Channel3");
        Channel channel4 = new Channel("Channel4");
        Channel channel5 = new Channel("Channel5");
        Channel[] channels = {channel1, channel2, channel3, channel4, channel5};
        tv.setChannels(channels);

        Program program1 = new Program("Program1", "9:00");
        Program program2 = new Program("Program2", "10:00");
        Program program3 = new Program("Program3", "11:00");
        Program[] programs1 = {program1, program2, program3};
        channel1.setPrograms(programs1);

        controller.turnOnTv(tv);
        tv.addChannel(channel1);
        tv.turnOnChannelNumber();
        channel1.addProgram(program1);
        channel1.showProgramName();
        controller.switchChannel();

    }
}
