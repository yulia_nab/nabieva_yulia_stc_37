public class RemoteController {
    private Tv tv;
    public void setTv(Tv tv) {
        this.tv = tv;
    }
    public void turnOnTv(Tv tv) {
        this.tv = tv;
        System.out.println("RemoteController turned " + this.tv.getBrand() + " Tv on");
    }

    public void switchChannel() {
        tv.turnOnChannelNumber();
        System.out.println("RemoteController turned next channel on");
    }
}
