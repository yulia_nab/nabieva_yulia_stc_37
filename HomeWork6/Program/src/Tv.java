public class Tv {

    private static final int MIN_DIAGONAL = 0;
    private final String brand;
    private int diagonal;
    private RemoteController remoteController;
    private Channel[] channels;
    private int channelsCount;
    private int activeChannel;

    public Tv(String brand, int diagonal) {

        if (diagonal >= MIN_DIAGONAL) {
            this.diagonal = diagonal;
        } else {
            this.diagonal = diagonal;
        }
        this.brand = brand;
        this.channels = new Channel[4];
        this.channelsCount = 0;
    }

    public void setRemoteController(RemoteController controller) {
    }

    public void setChannels(Channel[] channels) {
        this.channels = channels;
    }

    public String getBrand() {
        return brand;
    }

    public void addChannel(Channel channel) {
        if (this.channelsCount < channels.length) {
            this.channels[channelsCount++] = channel;
        } else {
            System.out.println("All channels added");
        }
    }

    public void turnOnChannelNumber() {
        if (activeChannel < channelsCount) {
            System.out.println("Channel " + channels[activeChannel++].getNameOfChannel() + " turned on");
        }
    }
}


