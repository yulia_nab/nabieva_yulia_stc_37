public class Channel {

    private String nameOfChannel;

    private Tv tv;
    private Program[] programs;
    private int programsCount;
    private int activeProgram;

    public Channel(String nameOfChannel) {
        this.nameOfChannel = nameOfChannel;
        this.programs = new Program[2];
        this.programsCount = 0;
    }

    public String getNameOfChannel() {
        return nameOfChannel;
    }

    public void setPrograms(Program[] programs) {
        this.programs = programs;
    }

    public void addProgram(Program program) {
        if (this.programsCount < programs.length) {
            this.programs[programsCount++] = program;
        } else {
            System.out.println("All programs added");
        }
    }

    public void showProgramName() {
        if (activeProgram < programsCount) {
            System.out.println("Program " + programs[activeProgram++].getNameOfProgram() + " is on");
        }
    }
}
