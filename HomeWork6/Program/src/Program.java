public class Program {
    private String nameOfProgram;
    private String timeOfProgram;

    private Channel channel;

    public Program(String nameOfProgram, String timeOfProgram) {
        this.nameOfProgram = nameOfProgram;
        this.timeOfProgram = timeOfProgram;
    }

    public String getNameOfProgram() {
        return nameOfProgram;
    }

}
