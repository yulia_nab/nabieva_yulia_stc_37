public class Square extends Figure implements ScaleFigure,FigureMovement{

    public Square(double side) {
        super(side);
    }

    @Override
    public double getSquareAmount() {
        double squareAmount = Math.pow(this.side, 2);
        System.out.println("Square square ");
        return squareAmount;
    }

    @Override
    public double getPerimeterAmount() {
        double perimeterAmount = 4 * this.side;
        System.out.println("Square perimeter ");
        return perimeterAmount;
    }

    @Override
    public void move(double value) {
        this.side += value;
        System.out.println(" Figure has moved " + value + " points");
    }

    @Override
    public void scale(double value) {
        this.squareAmount *= value;
        System.out.println(" Figure has scaled " + value + " sizes");
    }
}
