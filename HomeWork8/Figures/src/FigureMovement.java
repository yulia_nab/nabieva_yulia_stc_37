@FunctionalInterface
public interface FigureMovement {
    void move(double value);
}
