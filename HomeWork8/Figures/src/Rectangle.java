public class Rectangle extends Square {

    private double side2;

    public Rectangle(double side, double side2) {
        super(side);
        this.side2 = side2;
    }

    @Override
    public double getSquareAmount() {
        double squareAmount = this.side * this.side2;
        System.out.println("Rectangle square ");
        return squareAmount;
    }

    @Override
    public double getPerimeterAmount() {
        double perimeterAmount = 2 * (this.side + this.side2);
        System.out.println("Rectangle perimeter ");
        return perimeterAmount;
    }
}
