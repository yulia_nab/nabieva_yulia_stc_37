public abstract class Figure {
    private static final double DEFAULT_SQUARE_AMOUNT = 0.1;
    private static final double DEFAULT_PERIMETER_AMOUNT = 0.1;
    protected double squareAmount = DEFAULT_SQUARE_AMOUNT;
    protected double perimeterAmount = DEFAULT_PERIMETER_AMOUNT;
    protected double side;

    public Figure(double side) {
        this.side = side;
    }

    public double getSquareAmount() {
        return squareAmount;
    }

    public double getPerimeterAmount() {
        return perimeterAmount;


    }
}
