@FunctionalInterface
public interface ScaleFigure {
    void scale(double value);
}
