public class Round extends Figure implements FigureMovement, ScaleFigure {
    
    public Round(double side) {
        super(side);
    }

    @Override
    public double getSquareAmount() {
        double squareAmount = (Math.pow(this.side, 2))* Math.PI;
        System.out.println("Round square ");
        return squareAmount;
    }
    @Override
    public double getPerimeterAmount() {
        double perimeterAmount = 2 * this.side * Math.PI;
        System.out.println("Round perimeter ");
        return perimeterAmount;
    }

    @Override
    public void move(double value) {
        this.side += value;
        System.out.println(" Figure has moved " + value + " points");
    }

    @Override
    public void scale(double value) {
        this.squareAmount *= value;
        System.out.println(" Figure has scaled " + value + " sizes");
    }
}
