public class Main {

    public static void main(String[] args) {
        Figure rectangle = new Rectangle(2, 3);
        Figure square = new Square(5);
        Figure round = new Round(5);
        Figure ellipse = new Ellipse(2, 3);

        Figure[] figures = {rectangle, square, round, ellipse};
        for (Figure figure : figures) {
            System.out.println(figure.getSquareAmount());
            System.out.println(figure.getPerimeterAmount());
        }

        ((Square) square).scale(2);
        ((Round) round).move(3);
        ((Rectangle) rectangle).scale(2);
        ((Ellipse) ellipse).move(1);
    }
}
