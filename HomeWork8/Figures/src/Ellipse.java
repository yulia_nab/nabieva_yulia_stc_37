public class Ellipse extends Round {

    private double side2;

    public Ellipse(double side, double side2) {
        super(side);
        this.side2 = side2;
    }

    @Override
    public double getSquareAmount() {
        double squareAmount = this.side * this.side2 * Math.PI;
        System.out.println("Ellipse square ");
        return squareAmount;
    }

    @Override
    public double getPerimeterAmount() {
        double perimeterAmount = 4 * (((Math.PI * this.side * this.side2) + Math.pow((Math.abs(this.side - this.side2)), 2)) / (this.side + this.side2));
        System.out.println("Ellipse perimeter ");
        return perimeterAmount;
    }

}
