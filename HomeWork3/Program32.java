import java.util.Scanner;

class Program32 {

	public static double function(double x, double s) {
	    double sech = 1 / Math.cosh(x); 
	    double squared = Math.pow(sech, 2);
	    return ((Math.pow(x, s)) * squared);
	}

	public static double Integral(double a, double b, double s, int n) {
	    double dx, x, sum4x, sum2x;
	    double integral = 0.0;

	    dx = (b-a) / n;
	    sum4x = 0.0;
	    sum2x = 0.0;
	    for (int i = 1; i < n; i += 2) {
	        x = a + i * dx;
	        sum4x += function(x,s);
	    }
	    for (int i = 2; i < n-1; i += 2) {
	        x = a + i * dx;
	        sum2x += function(x,s);
	    }
	    integral = function(a,s) + function(a,b);
	    integral = (dx / 3)*(integral + 4 * sum4x + 2 * sum2x);
	    return integral;

    }
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		double a = scanner.nextDouble();
		double b = scanner.nextDouble();
		double s = scanner.nextDouble();
		int n = scanner.nextInt();
		
		double result = Integral(a, b, s, n);
		
		System.out.println(result);
	}

}
