import java.util.Scanner;
import java.util.Arrays;

 class Program31{

 	public static void arraySum(int array[] ) {

        int arraySum = 0;
        double arrayAverage = 0;
		for (int i = 0 ; i < array.length; i++) {
			arraySum += array[i];
			arrayAverage = arraySum / array.length;
 		}
 		System.out.println("ArraySum : " + arraySum);
 		System.out.println("ArrayAverage : " + arrayAverage);
    }
    public static void arrayMirror(int array[]  ) {
		int arrayMirror[] = array;

		for ( int i = 0; i < array.length / 2; i++) {
 			int temp = array[i];
 			array[i] = array[array.length - i - 1];
 			array[array.length - i - 1] = temp;
 		}
		System.out.println(Arrays.toString(array));
 		return;
 	}
 	    public static void number(int copyArray[] ) { 
		int number = copyArray[0];
		System.out.println(Arrays.toString(copyArray));

	    for  ( int i = 1; i < copyArray.length; i++) {
	    	number = number * 10 + copyArray[i];
	    }
		System.out.println(number);
	}
	public static void value(int copyArray[] ) {

		int min = copyArray[0];
		int max = copyArray[0] ;
		int positionOfMin = 0 ;
		int positionOfMax = 0;
		for (int i = 0 ; i < copyArray.length; i++) {
			if (copyArray[i] < min) {
				min = copyArray[i];
				positionOfMin = i;
			}
			else if (copyArray[i] > max) {
				max = copyArray[i];
				positionOfMax = i;
			}
	 	}
	 	copyArray[positionOfMin] = max;
	 	copyArray[positionOfMax] = min;
	 	for (int value : copyArray) {
            System.out.print( + value + " ");
        }
        return;
    }
    public static void isSorted(int array[]) {
		boolean isSorted = false;
		while(!isSorted) {
        	isSorted = true;
        	for (int i = 0; i < array.length-1; i++) {
            	if(array[i] > array[i+1]){
                    isSorted = false;
                    int temp = array[i];
                    array[i] = array[i+1];
                    array[i+1] = temp;
             	}
            }
        }
	    System.out.println(Arrays.toString(array));  
	    return;
    } 
 		public static void main(String[] args) {
 	
 		Scanner scanner = new Scanner(System.in);
		int n = scanner.nextInt();
		int []array = new int[n];
		for (int i = 0 ; i < n; i++) {
			array[i] = scanner.nextInt();
		}
		int copyArray[] = array.clone();

	System.out.println(Arrays.toString(array));
	arraySum(array);
	System.out.println("Array to number : " );
	number(copyArray);
	System.out.println("ArrayMirror : " );
	arrayMirror(array);
	System.out.println("ArraySwapMinToMax : " );
	value(copyArray);
	System.out.println("\nArrayIsSorted : " );
	isSorted(copyArray);
	}
}