package ru.innopolis.stc37.nabieva.homework9;

public class Main {
    public static void main(String[] args) {
        int[] numbers = {582, 106, 148};
        String[] strings = {"s887dddd", "456idk", "idkwht"};
        NumbersAndStringsProcessor numbersAndStringsProcessor = new NumbersAndStringsProcessor(numbers, strings);

        NumbersProcess reverseNumbers = Main::reverseNumber;
        NumbersProcess removeZeros = Main::removeZero;
        NumbersProcess evenDigits = Main::makeEvenDigits;

        StringsProcess stringsReverse = string -> new StringBuilder(string).reverse().toString();
        StringsProcess stringsRemoveDigits = string -> string.replaceAll("\\d+.*", "");
        StringsProcess stringsToUpperCase = String::toUpperCase;

        numbersAndStringsProcessor.processNumbers(numbers, reverseNumbers);
        numbersAndStringsProcessor.processNumbers(numbers, removeZeros);
        numbersAndStringsProcessor.processNumbers(numbers, evenDigits);

        numbersAndStringsProcessor.processStrings(strings, stringsReverse);
        numbersAndStringsProcessor.processStrings(strings, stringsRemoveDigits);
        numbersAndStringsProcessor.processStrings(strings, stringsToUpperCase);

    }

    private static int reverseNumber(int number) {
        int processedNumber = 0;
        for (int i = number; i != 0; i /= 10) {
            processedNumber = processedNumber * 10 + i % 10;
        }
        return processedNumber;

    }

    private static int removeZero(int number) {
        int processedNumber = 0;
        String num = Integer.toString(number);
        String newNum = num.replaceAll("0", "");
        processedNumber = Integer.parseInt(newNum);

        return processedNumber;
    }

    private static int makeEvenDigits(int number) {
        int processedNumber = 0;
        for (int i = number; i != 0; i /= 10) {
            if ((number % 10) % 2 == 1) {
                number -= 1;
            }
            processedNumber = number;
        }
        return processedNumber;
    }
}
