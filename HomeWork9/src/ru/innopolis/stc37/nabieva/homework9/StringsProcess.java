package ru.innopolis.stc37.nabieva.homework9;

@FunctionalInterface
public interface StringsProcess {
    String process(String process);
}
