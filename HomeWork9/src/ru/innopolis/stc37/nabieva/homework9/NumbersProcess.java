package ru.innopolis.stc37.nabieva.homework9;

@FunctionalInterface
public interface NumbersProcess {
    int process(int number);
}
