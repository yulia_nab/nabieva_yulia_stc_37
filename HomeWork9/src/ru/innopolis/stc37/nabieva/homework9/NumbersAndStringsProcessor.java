package ru.innopolis.stc37.nabieva.homework9;

public class NumbersAndStringsProcessor {
    private final int[] numbers;
    private final String[] strings;

    public NumbersAndStringsProcessor(int[] numbers, String[] strings) {
        this.numbers = numbers;
        this.strings = strings;
    }

    public void processNumbers(int[] numbers, NumbersProcess processType) {
        for (int i = 0; i < numbers.length; i++) {
            numbers[i] = processType.process(numbers[i]);
            System.out.println(processType.process(numbers[i]));
        }
    }

    public void processStrings(String[] strings, StringsProcess processType) {
        for (int i = 0; i < strings.length; i++) {
            strings[i] = processType.process(strings[i]);
            System.out.println(processType.process(strings[i]));
        }
    }
}
