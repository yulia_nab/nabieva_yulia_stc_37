package ru.innopolis.stc37.nabieva;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        ClientReceiverThread clientReceiverThread = new ClientReceiverThread("localhost", 7777);
        clientReceiverThread.start();

        while (true) {
            String message = scanner.nextLine();
            clientReceiverThread.sendMessage(message);
        }
    }
}

