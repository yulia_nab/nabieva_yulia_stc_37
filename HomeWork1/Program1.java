class Program1 {
	public static void main(String[] args) {
		int number = 12345;
		final int BASE = 10;

		int digitsSum = 0;

		digitsSum += number % BASE;
		number /= BASE;
		digitsSum += number % BASE;
		number /= BASE;
		digitsSum += number % BASE;
		number /= BASE;
		digitsSum += number % BASE;
		number /= BASE;
		digitsSum += number % BASE;
		number /= BASE;
		
		System.out.println(digitsSum);

	}
}