class Program2New {
	public static void main(String[] args) {
		int number = 12345;
		int BASE = 2;
	
		long binaryNumber = 0;
		
		binaryNumber += number % BASE;

		number /= BASE;
		binaryNumber += number % BASE * 10;

		number /= BASE;
		binaryNumber += number % BASE * 100;
		
		number /= BASE;
		binaryNumber += number % BASE * 1_000;
		
		number /= BASE;
		binaryNumber += number % BASE * 10_000;
		
		number /= BASE;
		binaryNumber += number % BASE * 100_000;
		
		number /= BASE;
		binaryNumber += number % BASE * 1_000_000;

		number /= BASE;
		binaryNumber += number % BASE * 10_000_000;

		number /= BASE;
		binaryNumber += number % BASE * 100_000_000;

		number /= BASE;
		binaryNumber += number % BASE * 1_000_000_000;

		number /= BASE;
		binaryNumber += number % BASE * 10_000_000_000L;

		number /= BASE;
		binaryNumber += number % BASE * 100_000_000_000L;

		number /= BASE;
		binaryNumber += number % BASE * 1_000_000_000_000L;

		number /= BASE;
		binaryNumber += number % BASE * 10_000_000_000_000L;

		number /= BASE;
		binaryNumber += number % BASE * 100_000_000_000_000L;
	
		System.out.println(binaryNumber);
	}
}