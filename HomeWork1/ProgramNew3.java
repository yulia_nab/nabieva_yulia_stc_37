import java.util.Scanner;

class ProgramNew3 {
	public static void main(String[] args) {
		final int BASE = 10;
		int product = 1;
		Scanner scanner = new Scanner(System.in);
		int currentNumber = scanner.nextInt();


		while (currentNumber != 0) {
			int digitsSum = 0;
			int copyNumber = currentNumber;
			while (copyNumber != 0) {
				digitsSum += copyNumber % BASE;
				copyNumber /= BASE;
			}

			int factor = 2;
			int copyDigitsSum = digitsSum;
			while (factor * factor <= digitsSum) {
				if (digitsSum % factor == 0) {
					copyDigitsSum /= factor;
					break;
				}
				factor++;
			}
			if (digitsSum == copyDigitsSum) {
				product *= currentNumber;
			}
			currentNumber = scanner.nextInt();
		}
		System.out.println(product);
	}
}