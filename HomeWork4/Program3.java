class Program3{
    public static int fib(int number) {
        if (number == 0) {
            return 0;
        }
        return fib_iter(number, 1, 0, 1);
    }
    private static int fib_iter(int number, int k, int a, int b) {
        if (k == number) {
            return b;
        }
        return fib_iter(number, k + 1, b, a + b);
    }
    public static void main(String[] args) {
        for (int number = 0; number < 10; ++number) {
            System.out.println("fib(" + number + ") = " +  fib(number));
        }
    }
    
}
