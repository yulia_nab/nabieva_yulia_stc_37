import java.util.Scanner;
class Program1{
    static boolean powerOf2(int n) {
    	if ( n == 1) {
    		return true;
    	}
    	else if (n % 2 !=0 || n == 0) {
    		return false;
    	}
    	return powerOf2(n / 2);
    }
    public static void main(String[] args) {
       Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        
        String answer = powerOf2(a) ? "True" : "False" ;
        System.out.println(answer);
    }  
} 
