package ru.innopolis.stc37.nabieva.game.client.sockets;

import javafx.application.Platform;
import ru.innopolis.stc37.nabieva.game.client.controllers.MainController;
import ru.innopolis.stc37.nabieva.game.client.utils.GameUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class SocketClient extends Thread {
    private Socket socket;
    private PrintWriter toServer;
    private BufferedReader fromServer;

    private MainController controller;

    private GameUtils gameUtils;

    public SocketClient(MainController controller, String host, int port) {
        try {
            socket = new Socket(host, port);
            toServer = new PrintWriter(socket.getOutputStream(), true);
            fromServer = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            this.controller = controller;
            this.gameUtils = controller.getGameUtils();
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public void run() {
        while (true) {
            String messageFromServer;
            try {
                messageFromServer = fromServer.readLine();
                if (messageFromServer != null) {
                    switch (messageFromServer) {
                        case "left":
                            gameUtils.goLeft(controller.getEnemy());
                            break;
                        case "right":
                            gameUtils.goRight(controller.getEnemy());
                            break;
                        case "shot":
                            Platform.runLater(() -> gameUtils.createBulletFor(controller.getEnemy(), true));
                            break;
                    }
                }
            } catch (IOException e) {
                throw new IllegalStateException(e);
            }
        }
    }

    public void sendMessage(String message) {
        toServer.println(message);
    }

}
