package stc.ru.innopolis.stc37.nabieva.homework12;

import java.util.Objects;

public class Car implements Comparable<Car> {
    private Long carNumber;
    private String carModule;
    private String carColour;
    private int carRun;
    private int carCost;

    public Car(Long carNumber, String carModule, String carColour, int carRun, int carCost) {
        this.carNumber = carNumber;
        this.carModule = carModule;
        this.carColour = carColour;
        this.carRun = carRun;
        this.carCost = carCost;
    }

    public Long getCarNumber() {
        return carNumber;
    }

    public void setCarNumber(Long carNumber) {
        this.carNumber = carNumber;
    }

    public String getCarModule() {
        return carModule;
    }

    public void setCarModule(String carModule) {
        this.carModule = carModule;
    }

    public String getCarColour() {
        return carColour;
    }

    public void setCarColour(String carColour) {
        this.carColour = carColour;
    }

    public int getCarRun() {
        return carRun;
    }

    public void setCarRun(int carRun) {
        this.carRun = carRun;
    }

    public int getCarCost() {
        return carCost;
    }

    public void setCarCost(int carCost) {
        this.carCost = carCost;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Car car = (Car) o;
        return carRun == car.carRun &&
                carCost == car.carCost &&
                Objects.equals(carNumber, car.carNumber) &&
                Objects.equals(carModule, car.carModule) &&
                Objects.equals(carColour, car.carColour);
    }

    @Override
    public int hashCode() {
        return Objects.hash(carNumber, carModule, carColour, carRun, carCost);
    }

    @Override
    public String toString() {
        return "stc.ru.innopolis.stc37.nabieva.homework12.Car{" +
                "carNumber='" + carNumber + '\'' +
                ", carModule='" + carModule + '\'' +
                ", carColour='" + carColour + '\'' +
                ", carRun=" + carRun +
                ", carCost=" + carCost +
                '}';
    }

    @Override
    public int compareTo(Car that) {
        return Integer.compare(this.carCost, that.carCost);
    }
}