package stc.ru.innopolis.stc37.nabieva.homework12;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) {
        List<Car> cars = new ArrayList<>();
        Car car0 = new Car( 101L, "Camry", "black", 0, 500000 );
        Car car1 = new Car( 201L, "BMW", "blue", 700, 900000 );
        Car car2 = new Car( 236L, "Opel", "white", 900, 300000 );
        Car car3 = new Car( 334L, "Camry", "silver", 100, 750000 );
        Car car4 = new Car( 346L, "Mitsubishi ", "black", 0, 800000 );
        Car car5 = new Car( 464L, "Lada", "silver", 1500, 200000 );
        Car car6 = new Car( 467L, "Opel", "red", 200, 400000 );
        Car car7 = new Car( 585L, "Renault", "black", 800, 400000 );

        cars.add(car0);
        cars.add(car1);
        cars.add(car2);
        cars.add(car3);
        cars.add(car4);
        cars.add(car5);
        cars.add(car6);
        cars.add(car7);


        cars.stream().filter(car -> car.getCarRun() <= 0)
                .sorted(Comparator.comparing(Car::getCarRun))
                .map(Car::getCarNumber)
                .forEach(System.out::println);

        cars.stream().filter(car -> car.getCarColour().equals("black"))
                .sorted(Comparator.comparing(Car::getCarColour))
                .map(Car::getCarNumber)
                .forEach(System.out::println);

        cars.stream().filter(car -> car.getCarCost() > 700000 && car.getCarCost() < 800000)
                .sorted(Comparator.comparing(Car::getCarNumber))
                .map(Car::getCarNumber)
                .forEach(System.out::println);


        cars.stream().min(Comparator.comparing(Car::getCarCost))
                .map(Car::getCarColour)
                .ifPresent(System.out::println);












    }
}