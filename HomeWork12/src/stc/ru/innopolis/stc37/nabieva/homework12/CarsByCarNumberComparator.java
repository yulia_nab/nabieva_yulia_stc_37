package stc.ru.innopolis.stc37.nabieva.homework12;

import java.util.Comparator;

public class CarsByCarNumberComparator implements Comparator <Car> {

    @Override
    public int compare(Car o1, Car o2) {
        return Long.compare(o1.getCarCost(), o2.getCarCost());
    }
}
