public class Main {
    public static void main(String[] args) {
        User user =  new User.Builder()
                .setFirstName("Marsel")
                .setLastName("Sidikov")
                .setAge(26)
                .setWorker(true)
                .build();

        System.out.println(user);

    }
}
