create table game
(
	id bigserial primary key,
	dateTime timestamp,
	playerFirst varchar,
	playerSecond varchar,
	playerFirstShotsCount integer,
	playerSecondShotsCount integer,
	secondsGameTimeAmount time
);

create table player
(
  id bigserial primary key,
  ip varchar,
  name varchar,
  points integer,
  maxWinsCount integer,
  maxLosesCount integer
);
create table shot(
  id bigserial primary key,
  dateTime timestamp,
  game bigint,
  shooter varchar,
  target varchar
);