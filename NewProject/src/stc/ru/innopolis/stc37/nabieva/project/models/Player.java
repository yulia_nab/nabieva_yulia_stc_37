package stc.ru.innopolis.stc37.nabieva.project.models;

import java.util.Objects;

public class Player {

    private String id;
    private String name;
    private Integer points;
    private Integer maxWinsCount;
    private Integer maxLosesCount;

    public Player(String id, String name, Integer points, Integer maxWinsCount, Integer maxLosesCount) {
        this.id = id;
        this.name = name;
        this.points = points;
        this.maxWinsCount = maxWinsCount;
        this.maxLosesCount = maxLosesCount;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPoints() {
        return points;
    }

    public void setPoints(Integer points) {
        this.points = points;
    }

    public Integer getMaxWinsCount() {
        return maxWinsCount;
    }

    public void setMaxWinsCount(Integer maxWinsCount) {
        this.maxWinsCount = maxWinsCount;
    }

    public Integer getMaxLosesCount() {
        return maxLosesCount;
    }

    public void setMaxLosesCount(Integer maxLosesCount) {
        this.maxLosesCount = maxLosesCount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Player player = (Player) o;
        return Objects.equals(id, player.id) &&
                Objects.equals(name, player.name) &&
                Objects.equals(points, player.points) &&
                Objects.equals(maxWinsCount, player.maxWinsCount) &&
                Objects.equals(maxLosesCount, player.maxLosesCount);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, points, maxWinsCount, maxLosesCount);
    }
}
