package stc.ru.innopolis.stc37.nabieva.project.services;

import stc.ru.innopolis.stc37.nabieva.project.dto.StatisticDto;
import stc.ru.innopolis.stc37.nabieva.project.models.Game;
import stc.ru.innopolis.stc37.nabieva.project.models.Player;
import stc.ru.innopolis.stc37.nabieva.project.models.Shot;
import stc.ru.innopolis.stc37.nabieva.project.repository.GamesRepository;
import stc.ru.innopolis.stc37.nabieva.project.repository.PlayersRepository;
import stc.ru.innopolis.stc37.nabieva.project.repository.ShotsRepository;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Optional;

import static java.time.LocalDateTime.now;

public class GameServiceImpl implements GameService {

    private final PlayersRepository playersRepository;

    private final GamesRepository gamesRepository;

    private final ShotsRepository shotsRepository;
    private LocalDateTime startGame = now();
    private LocalDateTime start = startGame;

    public GameServiceImpl(PlayersRepository playersRepository, GamesRepository gamesRepository, ShotsRepository shotsRepository) {
        this.playersRepository = playersRepository;
        this.gamesRepository = gamesRepository;
        this.shotsRepository = shotsRepository;
    }

    @Override
    public Long startGame(String firstId, String secondId, String firstPlayerNickname, String secondPlayerNickname) {

        Optional<Player> first = checkIfExists(firstId, firstPlayerNickname);
        Optional<Player> second = checkIfExists(secondId, secondPlayerNickname);
        if (first.isPresent()) {
            first = Optional.of(first.get());
        }
        if (second.isPresent()) {
            second = Optional.of(second.get());
        }
        Game game = new Game(now(), first, second, 0, 0, 0L);
        gamesRepository.save(game);
        return game.getId();
    }

    private Optional<Player> checkIfExists(String id, String nickname) {
        final Optional<Player> optionalPlayer = playersRepository.findByNickname(nickname);
        Player player = null;
        if (optionalPlayer.isPresent()) {
            player = optionalPlayer.get();
        }
        if (player == null) {
            player = new Player(id, nickname, 0, 0, 0);
            playersRepository.save(player);
        } else {
            player.setId(id);
            playersRepository.update(player);
        }
        return optionalPlayer;
    }

    @Override
    public void shot(Long gameId, String shooterNickname, String targetNickname) {
        Optional<Player> shooter = playersRepository.findByNickname(shooterNickname);
        Optional<Player> target = playersRepository.findByNickname(targetNickname);
        Game game = gamesRepository.findById(gameId);

        if (shooter.isPresent()) {
            shooter = Optional.of(shooter.get());
        }

        if (target.isPresent()) {
            target = Optional.of(target.get());
        }

        Shot shot = new Shot(now(), game, shooter, target);

        shooter.get().setPoints(shooter.get().getPoints() + 1);
        if (game.getPlayerFirst().isPresent()) {
            game.getPlayerFirst().equals(shooterNickname);
            game.setPlayerFirstShotsCount(game.getPlayerFirstShotsCount() + 1);
        }
        if (game.getPlayerSecond().isPresent()) {
            game.getPlayerSecond().equals(shooterNickname);
            game.setPlayerSecondShotsCount(game.getPlayerSecondShotsCount() + 1);
        }
        playersRepository.update(shooter.get());
        playersRepository.update(target.get());
        gamesRepository.update(game);
        shotsRepository.save(shot);
    }


    @Override
    public StatisticDto finishGame(Long gameId) {
        LocalDateTime end = now();
        Game game = gamesRepository.findById(gameId);
        Long id = game.getId();
        Optional<Player> firstPlayer = game.getPlayerFirst();
        if (firstPlayer.isPresent()) {
            firstPlayer = Optional.of(firstPlayer.get());
        }

        Integer firstPlayerScore = firstPlayer.get().getPoints();
        Optional<Player> secondPlayer = game.getPlayerSecond();
        if (secondPlayer.isPresent()) {
            secondPlayer = Optional.of(secondPlayer.get());
        }
        Integer secondPlayerScore = secondPlayer.get().getPoints();
        Integer firstPlayerHits = game.getPlayerFirstShotsCount();
        Integer secondPlayerHits = game.getPlayerSecondShotsCount();
        boolean firstPlayerWins = firstPlayerScore > secondPlayerScore;
        Long gameDuration = Duration.between(start, end).getSeconds();
        return new StatisticDto(id, firstPlayer, secondPlayer, firstPlayerHits, firstPlayerScore, secondPlayerHits, secondPlayerScore, firstPlayerWins, gameDuration);
    }
}
