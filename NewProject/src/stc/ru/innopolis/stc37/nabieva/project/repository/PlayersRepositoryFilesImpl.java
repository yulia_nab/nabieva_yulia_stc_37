package stc.ru.innopolis.stc37.nabieva.project.repository;

import stc.ru.innopolis.stc37.nabieva.project.models.Player;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Optional;

import static java.lang.Integer.*;
import static java.util.Optional.*;

public class PlayersRepositoryFilesImpl implements PlayersRepository {
    private final String dbPlayers;
    private final String playersId;

    public PlayersRepositoryFilesImpl(String dbPlayers, String playersId) {
        this.dbPlayers = dbPlayers;
        this.playersId = playersId;
    }

    @Override
    public Optional<Player> findByNickname(String nickname) {
        try (BufferedReader reader = new BufferedReader(new FileReader(playersId))){
            String line;
            while((line = reader.readLine()) != null) {
                if(line.contains(nickname)) {
                    final String[] split = line.split("#");
                    return of(new Player(split[0], split[1],
                            parseInt(split[2]), parseInt(split[3]), parseInt(split[4])));
                }
            }
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
        return empty();

    }

    @Override
    public void save(Player player) {
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(dbPlayers, true));
            player.setId(String.valueOf(generatePlayersId()));

            writer.write(player.getName() + "%n" + "\n");
            writer.close();
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }


    private Long generatePlayersId() {
        try (BufferedReader reader = new BufferedReader(new FileReader(playersId))){
            String lastGeneratedPlayersIdAsString = reader.readLine();
            long idPlayer = Long.parseLong(lastGeneratedPlayersIdAsString);
            reader.close();

            BufferedWriter writer = new BufferedWriter(new FileWriter(playersId));
            writer.write(String.valueOf(idPlayer + 1));
            writer.close();
            return idPlayer;

        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public void update(Player player) {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(playersId));
            String newPlayer = reader.readLine();
            long idNewPlayer = Long.parseLong(newPlayer);
            reader.close();

            BufferedWriter writer = new BufferedWriter(new FileWriter(playersId));
            writer.write(String.valueOf(idNewPlayer));
            writer.close();
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }

    }
}
