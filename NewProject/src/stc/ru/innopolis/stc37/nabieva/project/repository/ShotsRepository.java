package stc.ru.innopolis.stc37.nabieva.project.repository;
import stc.ru.innopolis.stc37.nabieva.project.models.Shot;

public interface ShotsRepository {
    void save(Shot shot);
}
