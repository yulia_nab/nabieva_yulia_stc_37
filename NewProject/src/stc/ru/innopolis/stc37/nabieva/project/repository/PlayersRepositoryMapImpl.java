package stc.ru.innopolis.stc37.nabieva.project.repository;
import stc.ru.innopolis.stc37.nabieva.project.models.Player;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class PlayersRepositoryMapImpl implements PlayersRepository {

    private Map<String, Player> players;

    public PlayersRepositoryMapImpl() {
        players = new HashMap<>();
    }

    @Override
    public Optional<Player> findByNickname(String nickname) {
        return Optional.of(players.get(nickname));
    }

    @Override
    public void save(Player player) {
        players.put(player.getName(), player);
    }

    @Override
    public void update(Player player) {
        if (players.containsKey(player.getName())) {
            players.put(player.getName(), player);
        } else {
            System.err.println("Нельзя обновить несуществующего игрока");
        }
    }
}
