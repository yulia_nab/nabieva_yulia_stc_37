package stc.ru.innopolis.stc37.nabieva.project.repository;

import stc.ru.innopolis.stc37.nabieva.project.models.Game;

public interface GamesRepository {
    void save(Game game);

    Game findById(Long gameId);

    void update(Game game);
}
