package stc.ru.innopolis.stc37.nabieva.project.repository;
import stc.ru.innopolis.stc37.nabieva.project.models.Player;

import java.util.Optional;

public interface PlayersRepository {
    Optional<Player> findByNickname(String nickname);

    void save(Player player);

    void update(Player player);
}
