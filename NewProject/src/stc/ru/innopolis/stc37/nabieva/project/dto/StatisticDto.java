package stc.ru.innopolis.stc37.nabieva.project.dto;

import stc.ru.innopolis.stc37.nabieva.project.models.Player;
import stc.ru.innopolis.stc37.nabieva.project.services.GameService;

import java.util.Optional;


public class StatisticDto implements GameService {

    private Long gameId;
    private Optional<Player> firstPlayer;
    private Optional<Player> secondPlayer;
    private int firstPlayerHits;
    private int secondPlayerHits;
    private int firstPlayerScore;
    private int secondPlayerScore;
    private boolean firstPlayerWins;
    private Long gameDuration;

    public Long getGameId() {
        return gameId;
    }

    public Optional<Player> getFirstPlayer() {
        return firstPlayer;
    }

    public Optional<Player> getSecondPlayer() {
        return secondPlayer;
    }

    public int getFirstPlayerHits() {
        return firstPlayerHits;
    }

    public int getSecondPlayerHits() {
        return secondPlayerHits;
    }

    public int getFirstPlayerScore() {
        return firstPlayerScore;
    }

    public int getSecondPlayerScore() {
        return secondPlayerScore;
    }

    public boolean isFirstPlayerWins() {
        return firstPlayerWins;
    }

    private Long getGameDuration() {

        return gameDuration;
    }

    public StatisticDto(Long gameId, Optional<Player> firstPlayer, Optional<Player> secondPlayer, int firstPlayerHits, int secondPlayerHits, int firstPlayerScore, int secondPlayerScore, boolean firstPlayerWins, Long gameDuration) {
        this.gameId = gameId;
        this.firstPlayer = firstPlayer;
        this.secondPlayer = secondPlayer;
        this.firstPlayerHits = firstPlayerHits;
        this.secondPlayerHits = secondPlayerHits;
        this.firstPlayerScore = firstPlayerScore;
        this.secondPlayerScore = secondPlayerScore;
        this.firstPlayerWins = firstPlayerWins;
        this.gameDuration = gameDuration;
    }

    @Override
    public String toString() {
        return "Игра с id " +
                "gameId = " + gameId +
                ", Игрок 1:  " + firstPlayer.get().getName() + '\'' +
                ", попаданий: " + firstPlayerHits +
                ", всего очков = " + firstPlayerScore +
                ", Игрок 2: " + secondPlayer.get().getName() + '\'' +
                ", попаданий: " + secondPlayerHits +
                ", всего очков = " + secondPlayerScore +
                ", Победа: " + firstPlayerWins + " for " + firstPlayer.get().getName() +
                ",  Игра длилась: " + gameDuration +
                '}';
    }

    @Override
    public Long startGame(String firstId, String secondId, String firstPlayerNickname, String secondPlayerNickname) {
        return null;
    }

    @Override
    public void shot(Long gameId, String shooterNickname, String targetNickname) {

    }

    @Override
    public StatisticDto finishGame(Long gameId) {
        return null;
    }

}
