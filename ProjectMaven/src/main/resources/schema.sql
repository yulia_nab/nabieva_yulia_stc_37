create table player
(
id bigserial primary key,
ip varchar(100),
name varchar(100),
points integer,
maxWinsCount integer,
maxLosesCount integer
);

create table game
(
id bigserial primary key,
dateTime varchar(100),
playerFirst bigint,
playerSecond bigint,
playerFirstShotsCount integer,
playerSecondShotsCount integer,
secondsGameTimeAmount bigint,
foreign key (playerFirst) references player(id),
foreign key (playerSecond) references player(id),

);