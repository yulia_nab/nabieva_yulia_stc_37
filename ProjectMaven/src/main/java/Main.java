import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import ru.innopolis.stc37.nabieva.repositories.CustomDataSource;
import ru.innopolis.stc37.nabieva.repositories.GamesRepository;
import ru.innopolis.stc37.nabieva.repositories.GamesRepositoryJdbcImpl;

import javax.sql.DataSource;

public class Main {
    public static void main(String[] args) {
        HikariConfig configuration = new HikariConfig();
        configuration.setJdbcUrl("jdbc:postgresql://localhost:5432/gameDb");
        configuration.setDriverClassName("org.postgresql.Driver");
        configuration.setUsername("postgres");
        configuration.setPassword("5858989");
        configuration.setMaximumPoolSize(20);
        DataSource dataSource = new HikariDataSource(configuration);
        GamesRepository gamesRepository = new GamesRepositoryJdbcImpl(dataSource);
        System.out.println(gamesRepository.findById(1L));



    }
}
