import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import ru.innopolis.stc37.nabieva.dto.StatisticDto;
import ru.innopolis.stc37.nabieva.repositories.GamesRepository;
import ru.innopolis.stc37.nabieva.repositories.GamesRepositoryJdbcImpl;
import ru.innopolis.stc37.nabieva.repositories.PlayersRepository;
import ru.innopolis.stc37.nabieva.repositories.PlayersRepositoryJdbcImpl;
import ru.innopolis.stc37.nabieva.repositories.ShotsRepository;
import ru.innopolis.stc37.nabieva.repositories.ShotsRepositoryJdbcImpl;
import ru.innopolis.stc37.nabieva.server.GameServer;
import ru.innopolis.stc37.nabieva.services.GameService;
import ru.innopolis.stc37.nabieva.services.GameServiceImpl;

import javax.sql.DataSource;
import java.time.LocalDateTime;

import static java.time.LocalDateTime.now;


public class MainServer {
    public static void main(String[] args) {
        HikariConfig configuration = new HikariConfig();
        configuration.setJdbcUrl("jdbc:postgresql://localhost:5432/gameDb");
        configuration.setDriverClassName("org.postgresql.Driver");
        configuration.setUsername("postgres");
        configuration.setPassword("5858989");
        configuration.setMaximumPoolSize(20);
        DataSource dataSource = new HikariDataSource(configuration);

        GamesRepository gamesRepository = new GamesRepositoryJdbcImpl(dataSource);
        PlayersRepository playersRepository = new PlayersRepositoryJdbcImpl(dataSource);
        ShotsRepository shotsRepository = new ShotsRepositoryJdbcImpl(dataSource);


        GameService gameService = new GameServiceImpl(playersRepository, gamesRepository, shotsRepository);
        Long gameId = gameService.startGame("127.0.0.1", "127.0.0.2", "Marsel", "Nemarsel");
        GameServer gameServer = new GameServer(gameService);
        gameServer.start(7777);


        StatisticDto statistic = gameService.finishGame(gameId,System.currentTimeMillis());

        System.out.println(statistic);


    }

}
