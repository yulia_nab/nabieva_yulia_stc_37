package ru.innopolis.stc37.nabieva.repositories;

import ru.innopolis.stc37.nabieva.models.Shot;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;

public class ShotsRepositoryJdbcImpl implements ShotsRepository  {
    //language=SQL
    private static final String SQL_INSERT_SHOT = "insert into " +
            "shot(datetime, game, shooter, target) values (?, ?, ?, ?)";


    private DataSource dataSource;

    public ShotsRepositoryJdbcImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void save(Shot shot) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_INSERT_SHOT, Statement.RETURN_GENERATED_KEYS)) {
            statement.setString(1, shot.getDateTime().toString());
            statement.setLong(2, shot.getId());
            statement.setLong(3, shot.getShooter().getId());
            statement.setLong(4, shot.getTarget().getId());
            int affectedRows = statement.executeUpdate();

            if (affectedRows != 1) {
                throw new SQLException("Can't insert");
            }

            try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
                if (generatedKeys.next()) {

                    Long id = generatedKeys.getLong("id");
                    shot.setId(id);
                } else {
                    throw new SQLException(("Can't retrieve id"));
                }

            } catch (SQLException e) {
                throw new IllegalStateException(e);
            }

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

}
