package ru.innopolis.stc37.nabieva.repositories;

import ru.innopolis.stc37.nabieva.models.Player;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class PlayersRepositoryJdbcImpl implements PlayersRepository {

    private DataSource dataSource;

    public PlayersRepositoryJdbcImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    //language=SQL
    private static final String SQL_FIND_PLAYER_BY_NICKNAME = "select id, ip, name from player where name = ?";
    //language=SQL
    private static final String SQL_INSERT_PLAYER = "insert into " +
            "player(ip, name) values (?, ?)";
    //language=SQL
    private static final String SQL_UPDATE_PLAYER_BY_ID = "update player set " +
            " ip = ?, name = ? where id = ?";

    private RowMapper<Player> playerRowMapper = row -> Player.builder()
            .id(row.getLong("id"))
            .ip(row.getString("ip"))
            .name(row.getString("name"))
            .build();

    @Override
    public Player findByNickname(String nickname) {
        try(Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(SQL_FIND_PLAYER_BY_NICKNAME);) {
            statement.setString(1, nickname);

            try(ResultSet rows = statement.executeQuery()) {
                if (rows.next()) {
                    return playerRowMapper.mapRow(rows);
                }
            }
            return null;
        }catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void save(Player player) {
        try(Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(SQL_INSERT_PLAYER, Statement.RETURN_GENERATED_KEYS)) {
            statement.setString(1, player.getIp());
            statement.setString(2, player.getName());
            statement.executeUpdate();

            int affectedRows = statement.executeUpdate();

            if (affectedRows != 1) {
                throw new SQLException("Can't insert");
            }
            try(ResultSet generatedKeys = statement.getGeneratedKeys()) {
                if(generatedKeys.next()){
                    Long id = generatedKeys.getLong("id");
                    player.setId(id);
                }else {
                    throw new SQLException("Can't retrieve id");
                }
            }catch (SQLException e) {
                throw new IllegalArgumentException(e);
            }

        }catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void update(Player player) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_PLAYER_BY_ID)) {
            statement.setString(1, player.getIp());
            statement.setString(2, player.getName());
            statement.setLong(3, player.getId());

            int affectedRows = statement.executeUpdate();

            if (affectedRows != 1) {
                throw new SQLException("Can't update");
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
