package ru.innopolis.stc37.nabieva.repositories;


import ru.innopolis.stc37.nabieva.models.Shot;

public interface ShotsRepository {
    void save(Shot shot);
}
