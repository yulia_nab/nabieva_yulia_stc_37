package ru.innopolis.stc37.nabieva.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Objects;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder

public class Player {

    private String ip;
    private long id;
    private String name;
    private Integer points;
    private Integer maxWinsCount;
    private Integer maxLosesCount;

}
