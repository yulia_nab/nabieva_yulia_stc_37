package ru.innopolis.stc37.nabieva.repositories;


import ru.innopolis.stc37.nabieva.models.Game;

public interface GamesRepository {
    void save(Game game);

    Game findById(Long gameId);

    void update(Game game);
}
