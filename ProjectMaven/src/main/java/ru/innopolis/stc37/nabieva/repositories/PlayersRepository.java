package ru.innopolis.stc37.nabieva.repositories;


import ru.innopolis.stc37.nabieva.models.Player;

public interface PlayersRepository {
    Player findByNickname(String nickname);

    void save(Player player);

    void update(Player player);
}
