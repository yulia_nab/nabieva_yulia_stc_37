package ru.innopolis.stc37.nabieva.repositories;

import ru.innopolis.stc37.nabieva.models.Game;
import ru.innopolis.stc37.nabieva.models.Player;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;

public class GamesRepositoryJdbcImpl implements GamesRepository {

    //language=SQL
    private static final String SQL_FIND_BY_ID = "select * from game where id = ?";
    //language=SQL
    private static final String SQL_INSERT_GAME = "insert into game (datetime, playerfirst, playersecond) " +
            " values (?, ?, ?);";
    //language=SQL
    private static final String SQL_UPDATE_GAME = "update game set secondsgametimeamount = ? where id = ?";


    static private final RowMapper<Game> gameRowMapper = row -> Game.builder()
            .id(row.getLong("id"))
            .dateTime(LocalDateTime.parse(row.getString("datetime")))
            .playerFirst(Player.builder()
                    .id(row.getLong("player_first"))
                    .build())
            .playerSecond(Player.builder()
                    .id(row.getLong("player_second"))
                    .build())
            .build();

    private DataSource dataSource;

    public GamesRepositoryJdbcImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void save(Game game) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_INSERT_GAME, Statement.RETURN_GENERATED_KEYS)) {
            statement.setString(1, game.getDateTime().toString());
            statement.setLong(2, game.getPlayerFirst().getId());
            statement.setLong(3, game.getPlayerSecond().getId());
            int affectedRows = statement.executeUpdate();

            if (affectedRows != 1) {
                throw new SQLException("Can't insert");
            }

            try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
                if (generatedKeys.next()) {

                    Long id = generatedKeys.getLong("id");
                    game.setId(id);
                } else {
                    throw new SQLException(("Can't retrieve id"));
                }

            } catch (SQLException e) {
                throw new IllegalStateException(e);
            }

        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }


    @Override
    public Game findById(Long gameId) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_FIND_BY_ID);) {
            statement.setLong(1, gameId);

            try (ResultSet rows = statement.executeQuery()) {
                if (rows.next()) {
                    return gameRowMapper.mapRow(rows);
                }
            }
            return null;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void update(Game game) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_GAME)) {
            statement.setLong(1, game.getSecondsGameTimeAmount());
            statement.setLong(2, game.getId());
            int affectedRows = statement.executeUpdate();

            if (affectedRows != 1) {
                throw new SQLException("Can't update");
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

}
