package ru.innopolis.stc37.nabieva.server;


import ru.innopolis.stc37.nabieva.services.GameService;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import static ru.innopolis.stc37.nabieva.server.CommandsParser.isMessageForDamage;
import static ru.innopolis.stc37.nabieva.server.CommandsParser.isMessageForExit;
import static ru.innopolis.stc37.nabieva.server.CommandsParser.isMessageForMove;
import static ru.innopolis.stc37.nabieva.server.CommandsParser.isMessageForNickname;
import static ru.innopolis.stc37.nabieva.server.CommandsParser.isMessageForShot;

public class GameServer {

    private ClientThread firstPlayer;
    private ClientThread secondPlayer;

    private ServerSocket serverSocket;

    private boolean isGameStarted = false;
    private long startTimeMills;
    private boolean isGameInProcess = true;
    private long gameId;
    private GameService gameService;
    private Lock lock = new ReentrantLock();

    public GameServer(GameService gameService) {
        this.gameService = gameService;
    }

    public void start(int port) {
        try {
            serverSocket = new ServerSocket(port);
            System.out.println("СЕРВЕР ЗАПУЩЕН...");
            System.out.println("ОЖИДАНИЕ ПОДКЛЮЧЕНИЯ ПЕРВОГО КЛИЕНТА...");
            firstPlayer = connect();
            System.out.println("ОЖИДАНИЕ ПОДКЛЮЧЕНИЯ ВТОРОГО КЛИЕНТА...");
            secondPlayer = connect();
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    private ClientThread connect() {
        Socket client;
        try {
               client = serverSocket.accept();
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
        ClientThread clientThread = new ClientThread(client);
        clientThread.start();
        System.out.println("КЛИЕНТ ПОДКЛЮЧЕН...");
        clientThread.sendMessage("Вы подключены к серверу");
        return clientThread;
    }

    private class ClientThread extends Thread {
        private final PrintWriter toClient;
        private final BufferedReader fromClient;

        private String playerNickname;
        private String ip;

        public ClientThread(Socket client) {
            try {
                this.toClient = new PrintWriter(client.getOutputStream(), true);

                this.fromClient = new BufferedReader(new InputStreamReader(client.getInputStream()));
                this.ip = client.getInetAddress().getHostAddress();
            } catch (IOException e) {
                throw new IllegalStateException(e);
            }
        }

        @Override
        public void run() {
            while (isGameInProcess) {
                String messageFromClient;
                try {
                    messageFromClient = fromClient.readLine();
                } catch (IOException e) {
                    throw new IllegalStateException(e);
                }
                if (messageFromClient != null) {
                    if (isMessageForNickname(messageFromClient)) {
                        resolveNickname(messageFromClient);
                    } else if (isMessageForExit(messageFromClient) && isGameInProcess) {
                        lock.lock();
                        gameService.finishGame(gameId, (System.currentTimeMillis() - startTimeMills) / 1000);
                        isGameInProcess = false;
                        lock.unlock();
                    } else if (isMessageForMove(messageFromClient)) {
                        resolveMove(messageFromClient);
                    } else if (isMessageForShot(messageFromClient)) {
                        resolveShot(messageFromClient);
                    } else if (isMessageForDamage(messageFromClient)) {
                        resolveDamage();
                    }
                }
                lock.lock();
                if (isReadyForStartGame()) {
                    gameId = gameService.startGame(firstPlayer.getIp(), secondPlayer.getIp(), firstPlayer.playerNickname, secondPlayer.playerNickname);
                    startTimeMills = System.currentTimeMillis();
                    isGameStarted = true;
                }
                lock.unlock();
            }
        }

        private void resolveDamage() {
            if (meFirst()) {
                gameService.shot(gameId, firstPlayer.playerNickname, secondPlayer.playerNickname);
            } else {
                gameService.shot(gameId, secondPlayer.playerNickname, firstPlayer.playerNickname);
            }
        }


        private void resolveShot(String messageFromClient) {
            if (meFirst()) {
                secondPlayer.sendMessage(messageFromClient);
            } else {
                firstPlayer.sendMessage(messageFromClient);
            }
        }

        private void resolveMove(String messageFromClient) {
            if (meFirst()) {
                secondPlayer.sendMessage(messageFromClient);
            } else {
                firstPlayer.sendMessage(messageFromClient);
            }
        }

        private void resolveNickname(String messageFromClient) {
            if (meFirst()) {
                fixNickname(messageFromClient, firstPlayer, "ИМЯ ПЕРВОГО ИГРОКА: ", secondPlayer);
            } else {
                fixNickname(messageFromClient, secondPlayer, "ОТ ВТОРОГО ИГРОКА: ", firstPlayer);
            }
        }

        private boolean isReadyForStartGame() {
            return firstPlayer.playerNickname != null && secondPlayer.playerNickname != null && !isGameStarted;
        }

        private void fixNickname(String nickname, ClientThread currentPlayer,
                                 String anotherMessagePrefix, ClientThread anotherPlayer) {
            currentPlayer.playerNickname = nickname.substring(6);
            System.out.println(anotherMessagePrefix + nickname);
            anotherPlayer.sendMessage(nickname);
        }

        public void sendMessage(String message) {
            toClient.println(message);
        }

        private boolean meFirst() {
            return this == firstPlayer;
        }

        public String getIp() {
            return ip;
        }
    }
}
