package ru.innopolis.stc37.nabieva.services;

import ru.innopolis.stc37.nabieva.dto.StatisticDto;
import ru.innopolis.stc37.nabieva.models.Game;
import ru.innopolis.stc37.nabieva.models.Player;
import ru.innopolis.stc37.nabieva.models.Shot;
import ru.innopolis.stc37.nabieva.repositories.GamesRepository;
import ru.innopolis.stc37.nabieva.repositories.PlayersRepository;
import ru.innopolis.stc37.nabieva.repositories.ShotsRepository;

import java.time.Duration;
import java.time.LocalDateTime;

import static java.time.LocalDateTime.now;

public class GameServiceImpl implements GameService {

    private PlayersRepository playersRepository;

    private GamesRepository gamesRepository;

    private ShotsRepository shotsRepository;
    private LocalDateTime startGame = now();
    private LocalDateTime start = startGame;
    private long startTimeMills;
    public GameServiceImpl(PlayersRepository playersRepository, GamesRepository gamesRepository, ShotsRepository shotsRepository) {
        this.playersRepository = playersRepository;
        this.gamesRepository = gamesRepository;
        this.shotsRepository = shotsRepository;
    }

    @Override
    public Long startGame(String firstIp, String secondIp, String firstPlayerNickname, String secondPlayerNickname) {

        System.out.println("ПОЛУЧИЛИ" + firstIp + " " + " " + secondIp + " " + " " + firstPlayerNickname + " " + " " + secondPlayerNickname);
        Player first = checkIfExists(firstIp, firstPlayerNickname);
        Player second = checkIfExists(secondIp, secondPlayerNickname);
        Game game = Game.builder()
                .dateTime(LocalDateTime.now())
                .playerFirst(first)
                .playerSecond(second)
                .playerFirstShotsCount(0)
                .playerSecondShotsCount(0)
                .secondsGameTimeAmount(0L)
                .build();
        gamesRepository.save(game);
        return game.getId();
    }

    private Player checkIfExists(String ip, String nickname) {
        Player player = playersRepository.findByNickname(nickname);
        if (player == null) {
            player = Player.builder()
                    .ip(ip)
                    .name(nickname)
                    .build();

            playersRepository.save(player);
        } else {
            player.setIp(ip);
            playersRepository.update(player);
        }
        return player;
    }

    @Override
    public void shot(Long gameId, String shooterNickname, String targetNickname) {
        Player shooter = playersRepository.findByNickname(shooterNickname);
        Player target = playersRepository.findByNickname(targetNickname);
        Game game = gamesRepository.findById(gameId);
        Shot shot = Shot.builder()
                .dateTime(LocalDateTime.now())
                .game(game)
                .shooter(shooter)
                .target(target)
                .build();
        shooter.setPoints(shooter.getPoints() + 1);

        if (game.getPlayerFirst().getName().equals(shooterNickname)) {
            game.setPlayerFirstShotsCount(game.getPlayerFirstShotsCount() + 1);
        }
        if (game.getPlayerSecond().getName().equals(shooterNickname)) {
            game.setPlayerSecondShotsCount(game.getPlayerSecondShotsCount() + 1);
        }
        playersRepository.update(shooter);
        gamesRepository.update(game);
        shotsRepository.save(shot);
    }


    @Override
    public StatisticDto finishGame(Long gameId, long seconds) {
            Game game = gamesRepository.findById(gameId);
            game.setSecondsGameTimeAmount(seconds);
            gamesRepository.update(game);

        LocalDateTime end = now();
        game = gamesRepository.findById(gameId);
        Long id = game.getId();
        Player firstPlayer = game.getPlayerFirst();
        Integer firstPlayerScore = firstPlayer.getPoints();
        Player secondPlayer = game.getPlayerSecond();
        Integer secondPlayerScore = secondPlayer.getPoints();
        Integer firstPlayerHits = game.getPlayerFirstShotsCount();
        Integer secondPlayerHits = game.getPlayerSecondShotsCount();
        boolean firstPlayerWins = firstPlayerScore > secondPlayerScore;
        Long gameDuration = Duration.between(start, end).getSeconds();
        return new StatisticDto(id, firstPlayer, secondPlayer, firstPlayerHits, firstPlayerScore, secondPlayerHits, secondPlayerScore, firstPlayerWins, gameDuration);


    }
}
