package stc.ru.innopolis.stc37.nabieva.project.app;

import stc.ru.innopolis.stc37.nabieva.project.dto.StatisticDto;
import stc.ru.innopolis.stc37.nabieva.project.repository.GamesRepository;
import stc.ru.innopolis.stc37.nabieva.project.repository.GamesRepositoryListImpl;
import stc.ru.innopolis.stc37.nabieva.project.repository.PlayersRepository;
import stc.ru.innopolis.stc37.nabieva.project.repository.PlayersRepositoryFilesImpl;
import stc.ru.innopolis.stc37.nabieva.project.repository.ShotsRepository;
import stc.ru.innopolis.stc37.nabieva.project.repository.ShotsRepositoryFileImpl;
import stc.ru.innopolis.stc37.nabieva.project.services.GameService;
import stc.ru.innopolis.stc37.nabieva.project.services.GameServiceImpl;

import java.util.Random;
import java.util.Scanner;


public class Main {

    public static final String SHOTS_DB_TXT = "C:\\Users\\User\\Desktop\\yulia\\nabieva_yulia_stc_37\\NewProject\\src\\stc\\ru\\innopolis\\stc37\\nabieva\\project\\resources\\shots_db.txt";
    public static final String SHOTS_SEQUENCE_TXT = "C:\\Users\\User\\Desktop\\yulia\\nabieva_yulia_stc_37\\NewProject\\src\\stc\\ru\\innopolis\\stc37\\nabieva\\project\\resources\\shots_sequence.txt";
    public static final String PLAYERS_DB_TXT = "C:\\Users\\User\\Desktop\\yulia\\nabieva_yulia_stc_37\\NewProject\\src\\stc\\ru\\innopolis\\stc37\\nabieva\\project\\resources\\players_db.txt";
    public static final String PLAYERS_ID_TXT = "C:\\Users\\User\\Desktop\\yulia\\nabieva_yulia_stc_37\\NewProject\\src\\stc\\ru\\innopolis\\stc37\\nabieva\\project\\resources\\players_id.txt";

    public static void main(String[] args) {

//        PlayersRepository playersRepository = new PlayersRepositoryMapImpl();
        PlayersRepository playersRepository = new PlayersRepositoryFilesImpl(PLAYERS_DB_TXT, PLAYERS_ID_TXT);
        GamesRepository gamesRepository = new GamesRepositoryListImpl();
        ShotsRepository shotsRepository = new ShotsRepositoryFileImpl(SHOTS_DB_TXT,
                SHOTS_SEQUENCE_TXT);
        GameService gameService = new GameServiceImpl(playersRepository, gamesRepository, shotsRepository);

        Scanner scanner = new Scanner(System.in);

        String first = scanner.nextLine();
        String second = scanner.nextLine();
        Random random = new Random();

        Long gameId = gameService.startGame("127.0.0.1", "127.0.0.2", first, second);
        String shooter = first;
        String target = second;
        int i = 0;
        while (i < 10) {

            System.out.println(shooter + " делайте выстрел в " + target);
            scanner.nextLine();

            int success = random.nextInt(2);

            if (success == 0) {
                System.out.println("Успешно!");
                gameService.shot(gameId, shooter, target);
            } else {
                System.out.println("Промах!");
            }

            String temp = shooter;
            shooter = target;
            target = temp;
            i++;
        }

        StatisticDto statistic = gameService.finishGame(gameId);

        System.out.println(statistic);
    }
}
