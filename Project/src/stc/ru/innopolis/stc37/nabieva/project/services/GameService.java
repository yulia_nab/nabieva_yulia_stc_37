package stc.ru.innopolis.stc37.nabieva.project.services;
import stc.ru.innopolis.stc37.nabieva.project.dto.StatisticDto;
public interface GameService {

    Long startGame(String firstIp, String secondIp, String firstPlayerNickname, String secondPlayerNickname);

    void shot(Long gameId, String shooterNickname, String targetNickname);

    StatisticDto finishGame(Long gameId);
}
