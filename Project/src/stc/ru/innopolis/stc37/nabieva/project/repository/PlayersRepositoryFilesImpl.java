package stc.ru.innopolis.stc37.nabieva.project.repository;

import stc.ru.innopolis.stc37.nabieva.project.models.Player;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class PlayersRepositoryFilesImpl implements PlayersRepository {
    private String dbplayers;
    private String playersid;

    public PlayersRepositoryFilesImpl(String dbplayers, String playersid) {
        this.dbplayers = dbplayers;
        this.playersid = playersid;
    }

    @Override
    public Player findByNickname(String nickname) {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(playersid));
            String lastGeneratedPlayersIdAsString = reader.readLine();
            nickname = String.valueOf(reader.readLine());
            return playersid.get(nickname);
            reader.close();

        } catch (IOException e) {
            throw new IllegalStateException(e);
        }

    }

    @Override
    public void save(Player player) {
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(dbplayers, true));
            player.setIp(String.valueOf(generatePlayersId()));

            writer.write(player.getName() + '\n');
            writer.close();
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }


    private Long generatePlayersId() {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(playersid));
            String lastGeneratedPlayersIdAsString = reader.readLine();
            Long idPlayer = Long.parseLong(lastGeneratedPlayersIdAsString);
            reader.close();

            BufferedWriter writer = new BufferedWriter(new FileWriter(playersid));
            writer.write(String.valueOf(idPlayer + 1));
            writer.close();
            return idPlayer;

        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public void update(Player player) {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(playersid));
            String newPlayer = reader.readLine();
            Long idNewPlayer = Long.parseLong(newPlayer);
            reader.close();

            BufferedWriter writer = new BufferedWriter(new FileWriter(playersid));
            writer.write(String.valueOf(idNewPlayer));
            writer.close();
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }

    }
}
