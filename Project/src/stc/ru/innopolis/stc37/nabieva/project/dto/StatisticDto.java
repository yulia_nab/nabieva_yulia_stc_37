package stc.ru.innopolis.stc37.nabieva.project.dto;

import stc.ru.innopolis.stc37.nabieva.project.models.Player;
import stc.ru.innopolis.stc37.nabieva.project.services.GameService;


public class StatisticDto implements GameService {

    private Long gameId;
    private Player firstPlayer;
    private Player secondPlayer;
    private int firstPlayerHits;
    private int secondPlayerHits;
    private int firstPlayerScore;
    private int secondPlayerScore;
    private boolean firstPlayerWins;
    private Long gameDuration;

    public Long getGameId() {
        return gameId;
    }

    public Player getFirstPlayer() {
        return firstPlayer;
    }

    public Player getSecondPlayer() {
        return secondPlayer;
    }

    public int getFirstPlayerHits() {
        return firstPlayerHits;
    }

    public int getSecondPlayerHits() {
        return secondPlayerHits;
    }

    public int getFirstPlayerScore() {
        return firstPlayerScore;
    }

    public int getSecondPlayerScore() {
        return secondPlayerScore;
    }

    public boolean isFirstPlayerWins() {
        return firstPlayerWins;
    }

    private Long getGameDuration() {

        return gameDuration;
    }

    public StatisticDto(Long gameId, Player firstPlayer, Player secondPlayer, int firstPlayerHits, int secondPlayerHits, int firstPlayerScore, int secondPlayerScore, boolean firstPlayerWins, Long gameDuration) {
        this.gameId = gameId;
        this.firstPlayer = firstPlayer;
        this.secondPlayer = secondPlayer;
        this.firstPlayerHits = firstPlayerHits;
        this.secondPlayerHits = secondPlayerHits;
        this.firstPlayerScore = firstPlayerScore;
        this.secondPlayerScore = secondPlayerScore;
        this.firstPlayerWins = firstPlayerWins;
        this.gameDuration = gameDuration;
    }

    @Override
    public String toString() {
        return "Игра с id " +
                "gameId = " + gameId +
                ", Игрок 1:  " + firstPlayer.getName() + '\'' +
                ", попаданий: " + firstPlayerHits +
                ", всего очков = " + firstPlayerScore +
                ", Игрок 2: " + secondPlayer.getName() + '\'' +
                ", попаданий: " + secondPlayerHits +
                ", всего очков = " + secondPlayerScore +
                ", Победа: " + firstPlayerWins + " for " + firstPlayer.getName() +
                ",  Игра длилась: " + gameDuration +
                '}';
    }

    @Override
    public Long startGame(String firstIp, String secondIp, String firstPlayerNickname, String secondPlayerNickname) {
        return null;
    }

    @Override
    public void shot(Long gameId, String shooterNickname, String targetNickname) {

    }

    @Override
    public StatisticDto finishGame(Long gameId) {
        return null;
    }

}
