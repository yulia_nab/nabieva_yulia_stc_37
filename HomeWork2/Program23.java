import java.util.Scanner;

 class Program23{
 	public static void main(String[] args) {
 		Scanner scanner = new Scanner(System.in);

		int n = scanner.nextInt();
		int []array = new int[n];
		double arrayAverage = 0;
		
		for (int i = 0 ; i < n; i++) {
			array[i] = scanner.nextInt();
			arrayAverage += array[i] ;
 		}
 		arrayAverage /=  array.length;
 		
		System.out.println(arrayAverage);
	}
}