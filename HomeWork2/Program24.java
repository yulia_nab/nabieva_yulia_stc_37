import java.util.Scanner;
import java.util.Arrays;

class Program24 {
 	public static void main(String[] args) {
 		Scanner scanner = new Scanner(System.in);

		int n = scanner.nextInt();
		int []array = new int[n];
		for (int i = 0 ; i < n; i++) {
			array[i] = scanner.nextInt();
		}
		int min = array[0];
		int max = array[0] ;
		int positionOfMin = 0 ;
		int positionOfMax = 0;

		System.out.println(Arrays.toString(array));	
		
		for (int i = 0 ; i < n; i++) {
			if (array[i] < min) {
				min = array[i];
				positionOfMin = i;
			}
			else if (array[i] > max) {
				max = array[i];
				positionOfMax = i;
			}
	 	}
	 	array[positionOfMin] = max;
	 	array[positionOfMax] = min;
	 	for (int value : array) {
            System.out.print(value + " ");
        }
	}
}