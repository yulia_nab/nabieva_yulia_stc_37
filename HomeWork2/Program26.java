import java.util.Scanner;
import java.util.Arrays;

class Program26 {
	public static void main(String args[]) {
		Scanner scanner = new Scanner(System.in);

		int n = scanner.nextInt();
		int []array = new int[n];
		for (int m = 0 ; m < n; m++) {
			array[m] = scanner.nextInt();
		}
		int number = array[0];
		System.out.println(Arrays.toString(array));

	    for  ( int i = 1; i < n; i++) {
	    	number = number * 10 + array[i];
	    }
		System.out.println(number);
	}
}