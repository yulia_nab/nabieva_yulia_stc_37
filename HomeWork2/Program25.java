import java.util.Scanner;
import java.util.Arrays;

class Program25 {
 	public static void main(String[] args) {
 		Scanner scanner = new Scanner(System.in);

		int n = scanner.nextInt();
		int []array = new int[n];
		for (int i = 0 ; i < n; i++) {
			array[i] = scanner.nextInt();
		}

        for ( int i = n - 1; i >= 1; i--) {
            for ( int j = 0; j < i ; j++) {
                if (array[j] > array[j+1]) {
                    int temp = array[j];
                    array[j] = array[j+1];
                    array[j+1] = temp; 
                }
            }
        }
        System.out.println(Arrays.toString(array));
    }
}