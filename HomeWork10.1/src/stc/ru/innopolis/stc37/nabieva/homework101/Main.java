package stc.ru.innopolis.stc37.nabieva.homework101;

public class Main {

    public static void main(String[] args) {

        InnoList list = new InnoArrayList();

        list.add(7);
        list.add(8);
        list.add(10);
        list.add(12);
        list.add(15);
        list.add(20);
        list.add(-77);
        list.add(100);

        InnoIterator iterator = list.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next() + " ");
        }
        list.contains(3);
        list.remove(8);
        list.removeByIndex(3);
        list.addToBegin(5);
        list.insert(6, 82);

        InnoIterator iterator2 = list.iterator();

        while (iterator2.hasNext()) {
            System.out.println(iterator2.next() + " ");
        }
    }
}
